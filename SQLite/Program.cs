﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLite
{
    class Program
    {
        static void Main(string[] args)
        {
            //string dbName = "myDB.sqlite";
            //SQLiteConnection.CreateFile(dbName);
            SQLiteConnection conn = new SQLiteConnection("Data Source=" + "myDB.sqlite" + ";Version=3;");
            conn.Open();
            string sql = "create table if not exists uyeler (id INTEGER PRIMARY KEY, name varchar(200), yas int)";
            SQLiteCommand command = new SQLiteCommand(sql, conn);
            command.ExecuteNonQuery();

            string sql1 = "insert into uyeler (id,name, yas) values (NULL,'Fatma ÜR', 28)";
            SQLiteCommand command1 = new SQLiteCommand(sql1, conn);
            command1.ExecuteNonQuery();

            SQLiteCommand cmd = conn.CreateCommand();
            cmd.CommandText = "select * from uyeler";
            cmd.ExecuteNonQuery();
            SQLiteDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                Console.WriteLine(dr["id"].ToString() + dr["name"].ToString() + dr["yas"].ToString());
            }
            conn.Close();
            Console.ReadLine();
        }
    }
}
